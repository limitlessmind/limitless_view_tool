
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "limitless_view_tool/version"

Gem::Specification.new do |spec|
  spec.name          = "limitless_view_tool"
  spec.version       = LimitlessViewTool::VERSION
  spec.authors       = ["Tony Serkis"]
  spec.email         = ["dev@limitlessmind.com"]

  spec.summary       = %q{View specific methods I use for my applications.}
  spec.description   = %q{Provides generated HTML for Rails applications.}
  spec.homepage      = "http://somedomain.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
end
